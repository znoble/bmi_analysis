import numpy as np
import pandas as pd
from sklearn.linear_model import LogisticRegression
from flask import g

def get_model():
    if 'model' not in g:
        ''' Creates our BMI LogReg Model '''
        bmi_df = pd.read_csv("../bmi.csv")
        X = bmi_df.loc[:, ['Height', 'Weight', 'Gender']]
        X['Gender'], _ = pd.factorize(X['Gender'], sort=True)
        X['Height_Weight'] = X['Height'] * X['Weight']
        X['Height_Gender'] = X['Height'] * X['Gender']
        X['Weight_Gender'] = X['Weight'] * X['Gender']
        X['Height2'] = X['Height']**2
        X['Weight2'] = X['Weight']**2
        X['Gender2'] = X['Gender']**2
        y = bmi_df.loc[:, 'Index']
        logreg = LogisticRegression(solver='liblinear').fit(X, y)
        g.model = logreg

    return g.model


def predict_bmi(model, height, weight, gender):
    indicies = ["Extremely Weak", "Weak", "Normal", "Overweight", "Obesity", "Extreme Obesity"]
    X = pd.DataFrame(np.array([[
        height, weight, gender,
        height*weight, height*gender, weight*gender,
        height**2, weight**2, gender**2
    ]]))
    print(X)
    X.columns = model.feature_names_in_
    return indicies[np.argmax(model.predict_proba(X))]


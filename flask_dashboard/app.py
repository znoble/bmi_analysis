from flask import (
    Flask, url_for, render_template, redirect, request
)
from utils.logreg import *

##
#  Using Logistic Regression, give predicted class based on inputs given
##


##
# App Creation
##

app = Flask(__name__)

with app.app_context():
    get_model()


@app.route("/", methods=("GET", "POST"))
def index():
    data = {}
    if request.method == "POST":
        get_model()
        height = int(request.form["height"])
        weight = int(request.form["weight"])
        gender = 1 if request.form["gender"] == "Male" else 0
        index = predict_bmi(g.model, height, weight, gender)
        data["index"] = index

    return render_template("bmi.html", data=data)
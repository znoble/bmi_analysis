Practice Project learning EDA, Linear Regression, and Logistic Regression.

Created a practice flask app to interact with Logistic Regression

Dataset used is from Kaggle:
    https://www.kaggle.com/yasserh/bmidataset

pipenv is used to manage python modules. To run flask app:
        pipenv install
to install all required modules. Then
        pipenv run flask run
from inside the flask_dashboard directory.


TODOS:
    Fix up CSS in app.
    Handle submitting with no values selected.
    add pipenv scrips to run flask application